drop database if exists antichaqueo ;
create database antichaqueo;
use antichaqueo;

create table tipoUsuario(
idTipoUsuario INT PRIMARY KEY,
nombre VARCHAR (15)
)ENGINE=innoDB;

create table usuario (
idUsuario INT PRIMARY KEY AUTO_INCREMENT,
idTipoUsuario INT,
primerNombre VARCHAR (15),
segundoNombre VARCHAR (15),
apellidoPaterno VARCHAR(15),
apellidoMaterno VARCHAR (15),
usuario VARCHAR(15),
password VARCHAR(15), 
activo bool,
FOREIGN KEY (idTipoUsuario) REFERENCES tipoUsuario (idTipoUsuario) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=innoDB;

CREATE TABLE incidente (
idIncidente INT PRIMARY KEY,
idUsuario INT,
fechaIncidente DATE,
alarma bool,
reporte LONGTEXT,
FOREIGN KEY  (idUsuario) REFERENCES usuario(idUsuario)
)ENGINE=innoDB;

CREATE TABLE patrullaje (
idPatrullaje INT PRIMARY KEY AUTO_INCREMENT,
idUsuario INT ,
fechaPatrullaje DATE,
reporte LONGTEXT,
FOREIGN KEY  (idUsuario) REFERENCES usuario(idUsuario)
)ENGINE=innoDB;

CREATE TABLE cordenadas(
idCordenadas INT PRIMARY KEY,
idPatrullaje INT,
latitud FLOAT,
longitud FLOAT,
inicial bool,
FOREIGN KEY (idPatrullaje) REFERENCES patrullaje(idPatrullaje)
)ENGINE=innoDB;

CREATE TABLE multimedia(
idMultimedia INT PRIMARY KEY,
idPatrullaje INT ,
archivo TEXT
)ENGINE=innoDB;

CREATE TABLE tipoDonacion(
idTipoDonacion INT PRIMARY KEY,
nombre VARCHAR(15)
)ENGINE=innoDB;

CREATE TABLE persona(
idPersona INT PRIMARY KEY ,
primerNombre VARCHAR(15),
segundoNombre VARCHAR (15),
apellidoPaterno VARCHAR (15),
apellidoMaterno VARCHAR (15),
nacionalidad VARCHAR (15),
numeroCuenta INT 
)ENGINE=innoDB;

CREATE TABLE donacion (
idDonacion INT PRIMARY KEY,
idPersona INT ,
idTipoDonacion INT,
monto FLOAT ,
FOREIGN KEY (idPersona) REFERENCES persona(idPersona),
FOREIGN KEY (idTipoDonacion) REFERENCES tipoDonacion(idTipoDonacion)
)ENGINE=innoDB;